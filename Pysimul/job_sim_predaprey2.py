"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_predaprey.py
```
"""
from pathlib import Path

import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.predaprey.solver import Simul

here = Path(__file__).absolute().parent

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.1
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs + -1.0)
sim.state.state_phys.set_var("Y", sim.Ys + -0.5)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XY()

fig_dir = here.parent / "fig"
fig_dir.mkdir(exist_ok=True)

# Note: if you want to modify the figure and/or save it, you can use
ax = plt.gca()
fig = ax.figure

fig.savefig(fig_dir / "fig_2.png")

sim.output.print_stdout.plot_XY_vs_time()

ax = plt.gca()
fig = ax.figure

fig.savefig(fig_dir / "fig_a2.png")

plt.show()


